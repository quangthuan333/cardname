<?php

// easily get cache with the ignore cache setting
// return empty when no cache found or caching turned off
function getCache($cache_key)
{
    if (empty($cache_key))
        return NULL;

    $CI =& get_instance();
    if ($CI->config->item("ignore_cache"))
        return NULL;

    //Autoloaded
    //$CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));

    //unique cache key for each project
    $cache_key = SERVER_HOST . "adMf93Oe9qw_" . $cache_key;
    
    $cached_data = $CI->cache->get($cache_key);
    return $cached_data;
}

// save cache, duration in seconds
function saveCache($cache_key, $data, $duration = 360)
{
    if (empty($cache_key))
        return;

    $CI =& get_instance();
    if ($CI->config->item("ignore_cache"))
        return;

    //Autoloaded
    //$CI->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));

    //unique cache key for each project
    $cache_key = SERVER_HOST . "adMf93Oe9qw_" . $cache_key;

    $CI->cache->save($cache_key, $data, $duration);
}

function deleteCache($cache_key)
{
    if (empty($cache_key))
        return NULL;
    
    //Autoloaded
    //$this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));

    //unique cache key for each project
    $cache_key = SERVER_HOST . "adMf93Oe9qw_" . $cache_key;

    return $this->cache->delete($cache_key);
}

//-------------------------------------------------------------

function getValueByKey($key, $default_value = NULL)
{
    $CI =& get_instance();
    return $CI->setting->getValueByKey($key, $default_value);
}

function calculate_distance_between_two_points($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $rad = M_PI / 180;
    
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad)
        * sin($latitudeTo * $rad) 
        + cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) 
        * cos($theta * $rad);

    $output = acos($dist) / $rad * 60 *  1.853 *1000 ; //returns in metres
    if (is_nan($output) || empty($output))
        return 0;

    return $output;
}

function log_api_response($api_name, $request_url, $http_status_code, $request_payload, $request_header, $response_payload, $response_header)
{
    $CI =& get_instance();
    $CI->load->model("external_api_logging");
    
    return $CI->external_api_logging->create(array(
        "api_name" => $api_name,
        "status_code" => $http_status_code,
        "request_url" => $request_url,
        "request_header" => $request_header,
        "request_payload" => $request_payload,
        "response_header" => $response_header,
        "response_payload" => $response_payload
    ));
}

//determine which language to use
function getLang($userOrUserId = null)
{
    $CI =& get_instance();

    // disable multi-language in system
    $multiLanguage = $CI->setting->getValueByKey('show_language_selector');
    if (!$multiLanguage)
        return "en";

    // use POST 'lang' variable, if submitted
    $allowed_lang = ['en', 'zh', 'ms', 'ta'];
    $post_lang = getHeader("lang");
    if (in_array($post_lang, $allowed_lang))
        return $post_lang;

    // accept either user model or user_id
    $user = is_array($userOrUserId) ? $userOrUserId : $CI->user->getById($userOrUserId);
    if (empty($user))
        return "en";

    // user lang is valid
    $lang = "en";
    if (isset($user['lang']))
        if (in_array($user['lang'], $allowed_lang))
            $lang = $user['lang'];

    return $lang;
}

function getHeader($headerName)
{
    if (empty($headerName))
        return NULL;

    $headerName = strtolower(trim($headerName));

    foreach ($_SERVER as $name => $value)
        if (strcasecmp(substr($name, 0, 5), 'HTTP_') == 0)
        {
            $key = str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))));
            if (strcasecmp(trim($key), $headerName) == 0)
                return $value;
        }

    return NULL;

    //not compatible with old version of PHP
    //$headers = getallheaders();
    //return isset($headers[$headerName]) ? $headers[$headerName] : null;
}

function getOrPostParam($paramName)
{
    $CI =& get_instance();
    $post_value = $CI->input->post($paramName, TRUE);
    if (!is_null($post_value))
        return $post_value;

    $get_value = $CI->input->get($paramName, TRUE);
    if (!is_null($get_value))
        return $get_value;

    return NULL;
}

function uploads_path($filename = '')
{
    $path = FCPATH . "../uploads/" . $filename;
    return $path;
}

function tmp_path($filename = '')
{
    $path = FCPATH . "../tmp/" . $filename;
    return $path;
}

function uploads_url($filename = '/namecard/uploads')
{
    //No data
    if (empty($filename))
         return '';

    //It is already a full HTTP URL, don't process further
    $startsWithHttp = strpos($filename, "http") === 0;
    if ($startsWithHttp)
         return $filename;

    //URL encode individual parts of filename (in case it contains '/')
    $filenamePartsCleaned = array();
    $filenameParts = explode("/", $filename);
    foreach ($filenameParts as $filenamePart)
        $filenamePartsCleaned[] = myUrlEncode($filenamePart);
    $filename = implode("/", $filenamePartsCleaned);

    $fullUrl = base_url('../uploads/' . $filename);

    //Clean up URLs by removing "../"
    $parts = explode("/", $fullUrl);
    $len = count($parts);
    $lastArr = array($parts[0] . "/", $parts[2]);
    for ($i = 3; $i < $len; $i++) {
         if ($parts[$i] == "..") {
               if ((count($lastArr)) >= 3) {
                    unset($lastArr[count($lastArr) - 1]) ;
               } 
         } else {
               $lastArr[count($lastArr)] = $parts[$i];
         }
    }
    $finalUrl = implode("/", $lastArr);

    // convert to http if isLocalhost
    $http_host = $_SERVER['HTTP_HOST'];
    $isLocalhost = strpos($http_host, "localhost") !== false;
    if ($isLocalhost)
        $finalUrl = preg_replace("/https:\/\//", "http://", $finalUrl);

    return $finalUrl;
}

function myUrlEncode($string) {
     $replace = array('%20');
     $search = array("+");
     return str_replace($search, $replace, urlencode($string));
}

//header format: array('Content-type: text/plain', 'Content-length: 100')
function performCurl($url, $post = array(), $headers = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_FAILONERROR, false);
    curl_setopt($ch, CURLOPT_HTTP200ALIASES, range(400, 599));

    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);    //seconds 
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);          //seconds

    if (!empty($headers))
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    if (!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
    }

    $raw_data = curl_exec($ch);
    $curl_error = curl_error($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $raw_data = substr($raw_data, $header_size);
    
    $jsonData = @json_decode($raw_data, true);

    if (empty($jsonData)){
        //log api response failure
        $post = json_encode($post);
        $header = json_encode($headers);
        log_api_response(
            "Generic",
            $url,
            $http_code,
            $post,
            $header,
            $raw_data,
            ""
        );
    }

    curl_close($ch);

    return $jsonData;
}


//----------------------------------------------------------------------------------------------------------------------
// 
//----------------------------------------------------------------------------------------------------------------------

function asset_url($filename = '')
{
    return base_url('assets/' . $filename);
}

function css_url($filename = '')
{
    return base_url('assets/css/' . $filename);
}

function js_url($filename = '')
{
    return base_url('assets/js/' . $filename);
}

function dist_url($filename = '')
{
    return base_url('dist/' . $filename);
}

function plugin_url($filename = '')
{
    return base_url('plugins/' . $filename);
}

function phantomjs_pdf_path()
{
    return phantomjs_path('pdf.js');
}

function phantomjs_path($js_file = 'pdf.js')
{
    $path = FCPATH . "../phantomjs/phantomjs";
    if (!empty($js_file))
        $path .=  " --ignore-ssl-errors=true " . FCPATH . "phantomjs/" . $js_file;
    return $path;
}

function notice_pdf_path($notice_id)
{
   $filename = "notice_" . $notice_id . ".pdf";
   $path = FCPATH . '../notice_reports/' . $filename;
   return $path;
}

function notice_pdf_url($notice_id)
{
    $filename = "notice_" . $notice_id . ".pdf";
    return base_url('../notice_reports/' . $filename);
}

function report_html_path($filename)
{
    $path = FCPATH . 'tmp/' . $filename;
    return $path;
}

function report_pdf_path($filename)
{
    $filename = substr_replace($filename ,"" , -5); // delete .html
    $path = FCPATH . 'tmp/' . $filename . '.pdf';
    return $path;
}


//----------------------------------------------------------------------------------------------------------------------
// To be moved to another helper file
//----------------------------------------------------------------------------------------------------------------------

function cronLog($message)
{
    $message = strip_tags($message);
    echo $message;
    echo "\n";
}

function debugLog($message, $isConsole = false)
{
   if (!$isConsole)
      $message = str_replace(" ", "&nbsp;", $message);
   else
      $message = strip_tags($message);

   echo $message;
   echo $isConsole ? "\n" : "<br/>";
}

function consoleLog($message)
{
   echo "<script>";
   echo "console.log('" . $message . "');";
   echo "</script>";
}

// Function to get the client IP address
function getClientIp()
{
    $ipaddress = NULL;
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];

    if ($ipaddress == '::1')
        $ipaddress = NULL;

    return $ipaddress;
}

//----------------------------------------------------------------------------------------------------------------------
// Resize image
//----------------------------------------------------------------------------------------------------------------------

function resizeImage($source_file, $target_width = 512, $target_file = NULL)
{
    //Sanity check
    if (!file_exists($source_file))
        return false;

    if (empty($target_width))
        $target_width = 512;
    
    // Replace original file
    if (empty($target_file))
        $target_file = $source_file;

    // Get new sizes
    //Severity: Warning (Not error)
    //getimagesize(): corrupt JPEG data: 18 extraneous bytes before marker
    $original_width = 0;
    $original_height = 0;
    try {
        list($tmp_width, $tmp_height) = getimagesize($source_file);
        $original_width = $tmp_width;
        $original_height = $tmp_height;
    } catch (Exception $e) {
        return false;
    }

    //Divide by zero
    if ($original_width <= 0 || $original_height <= 0)
        return false;

    $original_ratio = $original_width/$original_height;
    /*  
        if ($target_width/$target_height > $original_ratio) 
            $target_width = $target_height*$original_ratio;
        else 
            $target_height = $target_width/$original_ratio;
     */
    $target_height = $target_width/$original_ratio;
    
    //check mime type is JPG
    $real_file_type = strtolower(mime_content_type($source_file));
    $allowed_real_file_types = array(
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
    );
    $real_extension = array_search($real_file_type, $allowed_real_file_types);
    if (empty($real_extension))
        return false;

    try {
        //load
        $thumb = imagecreatetruecolor($target_width, $target_height);
        $source = imagecreatefromjpeg($source_file);
    
        //resize
        imagecopyresampled($thumb, $source, 0, 0, 0, 0, $target_width, $target_height, $original_width, $original_height);
        imagejpeg($thumb, $target_file, 80);
    } catch (Exception $e) {
        return false;
    }

    //Make sure file exists
    if (!file_exists($target_file))
        return false;

    return ;
}

?>