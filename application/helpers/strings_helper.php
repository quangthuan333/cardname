<?php

function startsWith($haystack, $needle) {
    if (empty($haystack) || empty($needle))
        return false;
    return strncmp($haystack, $needle, strlen($needle)) === 0;
}

function endsWith($haystack, $needle) {
    if (empty($haystack) || empty($needle))
        return false;
    return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
}

function isHttpUrl($haystack) {
    if (empty($haystack))
        return false;
    return startsWith($haystack, "http");
}

function isHttpsUrl($haystack)
{
    if (empty($haystack))
        return false;
    return startsWith($haystack, "https");
}

function isEmail($email)
{
    if (empty($email))
        return false;
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function contains($haystack, $needle)
{
    if (empty($haystack) || empty($needle))
        return false;
    $haystack = strtolower($haystack);
    $needle = strtolower($needle);
    return (strpos($haystack, $needle) !== false);
}

function containsWholeWord($haystack, $needle) {
    if (empty($haystack) || empty($needle))
        return false;
    return preg_match("/\b".$needle."\b/i", $haystack) ? true : false;
}

function arrayWithPropertyName($array, $key)
{
    if (empty($array))
        return null;

    $newArray = array();
    foreach ($array as $object)
        if (isset($object[$key]))
            $newArray[] = $object[$key];
        //$newArray[] = $object->{$key};

    return $newArray;
}

/**
 * Insert an element at the desired index
 * https://stackoverflow.com/questions/3797239/insert-new-item-in-array-on-any-position-in-php
 * @param array      $array
 * @param int|string $position
 * @param mixed      $new_element
 */
function arrayInsert(&$array, $position, $new_element)
{
    if (is_int($position)) {
        array_splice($array, $position, 0, $new_element);
        return;
    }

    $pos   = array_search($position, array_keys($array));
    $array = array_merge(
        array_slice($array, 0, $pos),
        $new_element,
        array_slice($array, $pos)
    );
}

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . 'GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . 'MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . 'KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . 'bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . 'byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function getLocalizedString($slug, $lang = 'en')
{
    $CI =& get_instance();
    return $CI->app_translation->getLocalizedString($slug, $lang);
}

function generateUUID() 
{
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

?>