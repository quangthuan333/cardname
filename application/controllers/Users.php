<?php
class Users extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index() {
        echo 'hello page';
    }
    //user
    public function user($id)
    {
        if (empty($id))
            return $this->response(['error' => "Missing ID parameter (8371)"]);
        
        $user = $this->User->getById($id);
        if (empty($user))
            return $this->response(['error' => "No user found with ID $id (4721)"]);

        $informations = $this->UserInformation->getWithUserId($id);
        if (empty($informations))
            return $this->response($user);
            
        //Inject informations into User model
        $user['informations'] = $informations;

        $user['homepage_url'] = "https://originally.us";
        return $this->response($user);
    }

    private function response($json)
    {
        return print_r(json_encode($json, true));
    }
}