<?php
/**
 * Created by PhpStorm.
 * User: calixto
 * Date: 1/17/19
 * Time: 1:25 PM
 */

class MY_Model1 extends CI_Model
{
    protected $table_name = null;
    protected $uploads_subfolder = null;
    protected $database_conn = null;
    protected $select_fields = null;

    function __construct()
    {
        parent::__construct();

        //Warning about missing table_name variable
        if (empty($this->table_name)) {
            $className = get_class($this);
            $error = "WARNING: Class '" . $className . "' is missing 'table_name' (Code: 9342)";
            $this->slack_library->send($error);
        }

        //Use secondary database connection, if any
        if (!empty($this->database_conn)) {
            $conn = $this->load->database($this->database_conn, true);
            if ($conn)
                $this->db = $conn;
        }
    }

    private function padPreUpdate(&$data)
    {
        if (empty($data))
            return;

        foreach ($data as $key => &$value)
            if (contains($key, "timestamp"))
                if ((is_numeric($value) && (int)$value == $value))
                    $value = date('Y-m-d H:i:s', $value);
    }

    function update($id, $data)
    {
        if (empty($data))
            return 0;

        $conditions = array("id" => $id);
        return $this->updateWithConditions($conditions, $data);
    }

    function create($data)
    {
        $this->padPreUpdate($data);

        //Use only keys present in table columns
        $fields = $this->listFieldNames();
        foreach ($fields as $param)
            if (isset($data[$param]))
                $this->db->set($param, $data[$param]);

        //Automatically set current timestamp, if not already set
        if (in_array("created_timestamp", $fields) && !isset($data["created_timestamp"]))
            $this->db->set("created_timestamp", "NOW()", FALSE);
        if (in_array("modified_timestamp", $fields) && !isset($data["modified_timestamp"]))
            $this->db->set("modified_timestamp", "NOW()", FALSE);
        if (in_array("updated_timestamp", $fields) && !isset($data["updated_timestamp"]))
            $this->db->set("updated_timestamp", "NOW()", FALSE);

        $success = $this->db->insert($this->table_name);
        if (!$success) {
            //$this->db->_error_message();      //this function doesn't exist in CI 3.x
            /*
            $err = $this->db->error();
            $err['code'];                       //need to sanity check before using these
            $err['message'];
            */

            $errors = $this->db->error();
            if (is_array($errors)) {
                foreach ($errors as $error)
                    echo $error;
            }
            else {
                echo $errors;
            }

            return false;
        }

        return $this->db->insert_id();
    }

    public function listFieldNames()
    {
        if (empty($this->table_name))
            return false;
        
        return $this->db->list_fields($this->table_name);
    }

    //------------------------------------------------------------

    protected function handleConditions($conditions)
    {
        if (empty($conditions))
            return;

        foreach ($conditions as $key => $value)
        {

            if (contains($key, "NOT IN")) {
                if (!empty($value)) {
                    $key = trim(str_replace("NOT IN", "", $key));
                    $this->db->where_not_in($key, $value);
                }
            }
            else if (contains($key, " IN")) {
                if (!empty($value)) {
                    $key = trim(str_replace(" IN", "", $key));
                    $this->db->where_in($key, $value);
                }
            }
            else if (contains($value, "IS NOT NULL")) {
                $this->db->where($key . " " . $value, NULL, FALSE);
            }
            else if (contains($value, "NOW()") || contains($value, "INTERVAL")) {
                $this->db->where($key, $value, FALSE);
            }
            else if (contains($value, "UNIX_TIMESTAMP")) {
                $this->db->where($key, $value, FALSE);
            }
            else if (contains($value, "DATE()")) {
                $this->db->where($key, $value, FALSE);
            }else if (contains($key, "_timestamp") && (!contains(strtolower($key), 'date'))){ // no special date formating
                if (empty($value))
                    $this->db->where($key, $value);
                else
                    $this->db->where($key, date('Y-m-d H:i:s', $value));
            }
            else {
                $this->db->where($key, $value, TRUE);
            }
        }
    }

    private function handleOrderByArray($orderByArray)
    {
        if (empty($orderByArray))
            return;

        foreach ($orderByArray as $key => $value)
            $this->db->order_by($key, $value);
    }

    //------------------------------------------------------------

    public function getAll($orderByArray = NULL)
    {
        if (empty($this->select_fields))            $this->db->select('*');
        else                                        $this->db->select(implode(",", $this->select_fields));

        $this->handleOrderByArray($orderByArray);

        return $this->internal_getThisTable();
    }

    public function getAllActive($orderByArray = NULL)
    {
        if (empty($this->select_fields))            $this->db->select('*');
        else                                        $this->db->select(implode(",", $this->select_fields));
        
        $this->db->where('active', 1);

        $this->handleOrderByArray($orderByArray);

        return $this->internal_getThisTable();
    }

    public function getRandom($count = 1, $massageModel = false)
    {
        $sql = "SELECT * FROM " . $this->table_name . " ORDER BY RAND() LIMIT $count";
        $query = $this->db->query($sql);

        $models = $this->rawSql($sql, $massageModel);
        if (!$models)
            return false;

        return $count == 1 ? $models[0] : $models;
    }

    //Should return only 1 model, not an array
    public function getById($id)
    {
        if (empty($this->select_fields))            $this->db->select('*');
        else                                        $this->db->select(implode(",", $this->select_fields));

        $this->db->where('id', $id);
        
        //perform query
        $models = $this->internal_getThisTable(false);        //no massage
        if (!$models)
            return false;

        $model = $models[0];
        $this->massageModel($model);

        return $model;
    }

    public function checkExistWithKeyAndValue($key, $value)
    {
        $conditions = array($key => $value);
        $count = $this->countWithConditions($conditions);
        return $count > 0;
    }

    public function countWithConditions($conditions = array())
    {
        $this->db->select('count(*) as count_D6i2h7');
        $this->handleConditions($conditions);

        //perform query
        $models = $this->internal_getThisTable(false);
        if (!$models)
            return false;

        if (empty($models))
            return 0;

        return $models[0]['count_D6i2h7'];
    }

    public function countWithConditionString($conditionsString)
    {
        $sql = "SELECT count(*) as count_E8XXQFDbjo";
        $sql .= " FROM `" . $this->table_name . "`";
        $sql .= " WHERE " . $conditionsString;

        //perform query
        $models = $this->internal_rawQuery($sql);
        if (!$models)
            return false;

        if (empty($models))
            return 0;

        return $models[0]['count_E8XXQFDbjo'];
    }

    public function sumWithConditions($key, $conditions = array())
    {
        $this->db->select('sum(' . $key . ') as sum_XFtbVYLipN');
        $this->handleConditions($conditions);

        //perform query
        $models = $this->internal_getThisTable(false);
        if (!$models)
            return false;

        if (empty($model))
            return 0;

        return $models['sum_XFtbVYLipN'];
    }

    public function getWithConditions($conditions = array(), $orderByArray = NULL, $massageModel = true)
    {
        if (empty($this->select_fields))            $this->db->select('*');
        else                                        $this->db->select(implode(",", $this->select_fields));

        $this->handleConditions($conditions);
        $this->handleOrderByArray($orderByArray);

        return $this->internal_getThisTable();
    }

    public function getWithConditionsAndLimit($conditions = array(), $page = 0, $limit = 10, $orderByArray = NULL)
    {
        if (empty($this->select_fields))            $this->db->select('*');
        else                                        $this->db->select(implode(",", $this->select_fields));

        if ($limit > 0)
            $this->db->limit($limit, $page * $limit);
        
        $this->handleOrderByArray($orderByArray);
        $this->handleConditions($conditions);

        return $this->internal_getThisTable();
    }

    public function getLastestById()
    {
        $limit = 1;
        $array = $this->getWithConditionsAndLimit(array(), 0, $limit, array("id" => "desc"));
        if (!empty($array))
            return $array[0];
        
        return NULL;
    }
    
    public function getLatestByTimestamp($limit = 10)
    {
        return $this->getWithConditionsAndLimit(array(), 0, $limit, array("modified_timestamp" => "desc"));
    }

    public function rawSql($sql, $massageModel = false)
    {
        $query = $this->db->query($sql);
        return $this->internal_handleQuery($query, $sql, $massageModel);
    }

    public function getDistinctWithConditionsAndLimit($distinct_col, $conditions = array(), $page = 0, $limit = 10, $orderByArray = NULL)
    {
        $this->db->select('DISTINCT (' . $distinct_col . ')');

        if ($limit > 0)
            $this->db->limit($limit, $page * $limit);

        $this->handleOrderByArray($orderByArray);
        $this->handleConditions($conditions);

        return $this->internal_getThisTable();
    }

    //Mainly for custom SQL used in feed()
    public function getWithConditionString($conditionsString)
    {
        $sql = "SELECT *";
        $sql .= " FROM `" . $this->table_name ."` WHERE " . $conditionsString;
        return $this->rawSql($sql, true);
    }

    //Mainly for custom SQL used in feed()
    public function getWithConditionStringAndLimit($conditionsString, $page = 0, $limit = 10, $orderByArray = NULL)
    {
        $sql = "SELECT *";
        $sql .= " FROM `" . $this->table_name ."` WHERE " . $conditionsString . ' ';
        if (!empty($orderByArray)) {
            $orderByString = "";
            foreach($orderByArray as $key => $value)
                $orderByString .= "$key $value, ";
            $orderByString = rtrim($orderByString, ", ");
            $sql .= " ORDER BY " . $orderByString . ' ';
        }
        $sql .= " LIMIT " . $page * $limit . ", $limit ";
        
        return $this->rawSql($sql, true);
    }


    //---------------

    public function createOrUpdate($data = array(), $key = 'id', $force_return_id = false)
    {
        if (empty($data))
            return -1;
        if (empty($key))
            $key = 'id';
        
        $id = null;
        if (isset($data[$key]) && !empty($data[$key]))
            $id = $data[$key];

        if (empty($id))
            return $this->create($data);

        $conditions = [$key => $id];
        $existModel = $this->getWithConditions($conditions);

        if (empty($existModel)) 
            return $this->create($data);

        $affected =  $this->updateWithConditions($conditions, $data);

        if (!$force_return_id)
            return $affected;

        $this_record = $this->getWithConditionsAndLimit($conditions, 0, 1);
        return $this_record[0]['id'];
    }

    //---------------

    public function updateWithConditions($conditions, $data = array())
    {
        if (empty($conditions))
            return;
        if (empty($data))
            return;
        $this->handleConditions($conditions);

        $this->padPreUpdate($data);

        $updated = false;
        $keys = array_keys($data);
        $db_keys = $this->listFieldNames();

        foreach ($keys as $key){ //only set to update if the key exists
            if (in_array($key,$db_keys)){
                $updated = true;
                $this->db->set($key, $data[$key]);
            }
        }

        if (in_array("modified_timestamp",$db_keys))
            $this->db->set("modified_timestamp", "NOW()", FALSE);
        if (in_array("updated_timestamp",$db_keys))
            $this->db->set("updated_timestamp", "NOW()", FALSE);

        $success = $this->db->update($this->table_name);
        $afftectedRows = $this->db->affected_rows();
        if ($success && $afftectedRows == 0)
            $afftectedRows = 1;

        return $afftectedRows;
    }

    //Increase or decrease a field without reading from it first
    public function increaseDecreaseFieldValue($id, $key, $offset_string_value = "+1")
    {
        $this->db->where('id', $id);
        $this->db->set($key, '' . $key . $offset_string_value, FALSE);
        $success = $this->db->update($this->table_name);
        $afftectedRows = $this->db->affected_rows();
        if ($success && $afftectedRows == 0)
            $afftectedRows = 1;
        return $afftectedRows;
    }

    //------------

    public function deleteById($id)
    {
        if (empty($id))
            return;

        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
        $affected = $this->db->affected_rows();
        return $affected;
    }

    public function deleteWithConditions($conditions = array())
    {
        if (empty($conditions))
            return;
        $this->handleConditions($conditions);

        $success = $this->db->delete($this->table_name);
        $afftectedRows = $this->db->affected_rows();
        if ($success && $afftectedRows == 0)
            $afftectedRows = 1;

        return $afftectedRows;
    }

    //-----------

    public function insert($data = array())
    {
        if (empty($data))
            return 0;

        $success = $this->db->insert_batch($this->table_name, $data);
        if ($success)
            $afftectedRows = $this->db->affected_rows();

        return $afftectedRows;
    }

    //----------

    public function uploadsFolder()
    {
        return $this->uploads_subfolder . "/";
    }

    public function relativeUploadsFolder()
    {
        return uploads_path($this->uploads_subfolder . '/');
    }

    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (!$length || $length == 0)
            return 0 ;
        if (substr($haystack, - $length) === $needle)
            return $haystack;
    }

    private function internal_getThisTable($massageModel = true)
    {
        $query = $this->db->get($this->table_name);
        $sql = $this->db->last_query();
        return $this->internal_handleQuery($query, $sql, $massageModel);
    }

    private function internal_handleQuery($query, $sql, $massageModel = true)
    {
        //check for low-level MySQL errors first
        $dbError = $this->db->error();
        $dbErrorMessage = $dbError['message'];
        if (!empty($dbErrorMessage))
        {
            $debug = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $callerFunction = isset($debug[1]['function']) ? $debug[1]['function'] : null;

            $error = $this->table_name . " $callerFunction error: " . $dbErrorMessage . "\n" . $sql;
            $this->slack_library->send($error);
            return false;
        }

        //generic query failure
        if (!$query)
            return false;

        $models = NULL;
        if (method_exists($query, 'result_array'))      //just to be safe
            $models = $query->result_array();

        if ($massageModel && !empty($models))
            foreach ($models as &$model)
                $this->massageModel($model);
        
        return $models;
    }

    public function massageModel(&$model = array())
    {
        $this->load->helper('utility_helper');

        foreach ($model as $key => $value)
        {
            if ($this->endsWith($key, "_file")) {
                $new_key = $key . "_url";
                if (!isset($model[$new_key]) || empty($model[$new_key]))
                {
                    if (!$value)
                        $model[$new_key] = "";
                    else if (startsWith($value, "http://") || startsWith($value, "https://"))
                        $model[$new_key] = uploads_url($value);
                    else if (empty($this->uploads_subfolder))
                        $model[$new_key] = uploads_url($value);
                    else
                        $model[$new_key] = uploads_url($this->uploads_subfolder . "/" . $value);
                }
            }

            if (!$value)
                continue;

            //Force phone number as string
            if ($this->endsWith($key, "phone")) {
                $model[$key] = $value . "\n";
            }

            else if ($this->endsWith($key, "_timestamp")) {
                if (!is_numeric($value) && !empty($value))
                    $model[$key] = strtotime($value);
            }
        }
        
        return $model;
    }


}