<?php
class MY_Model extends CI_Model
{
    protected $table_name = null;
    protected $database_conn = null;
    protected $select_fields = null;
    protected $uploads_subfolder = null;
    protected $sensitiveFields = null;
    function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $models = $this->db->get($this->table_name);
        if(empty($models))
            return NULL;

        $array = $models->result_array();
        return $array;
    }
     public function getById($id)
    {
        if (empty($id))
            return NULL;

        $this->db->where("id", $id);
        
        $models = $this->db->get($this->table_name);
        if (empty($models))
            return NULL;

        $array = $models->result_array();
        if (empty($array))
            return NULL;

        //massage model
        foreach ($array as &$model)
            $this->massageModel($model);

        return $array[0];
    }
    
    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (!$length || $length == 0)
            return 0 ;
        if (substr($haystack, - $length) === $needle)
            return $haystack;
    }

    protected function massageModel(&$model)
    {
        foreach ($model as $key => $value)
        {
            if ($this->endsWith($key, "_file")) {
                $new_key = $key . "_url";
                if (!isset($model[$new_key]) || empty($model[$new_key]))
                {
                    if (!$value)
                        $model[$new_key] = "";
                    else if (startsWith($value, "http://") || startsWith($value, "https://"))
                        $model[$new_key] = $value;
                    else if (empty($this->uploads_subfolder))
                        $model[$new_key] = base_url("uploads/") . $value;
                    else
                        $model[$new_key] = base_url("uploads/") . $this->uploads_subfolder . "/" . $value ;
                }
                unset($model[$key]);
            }

            if (!$value)
                continue;

            //Force phone number as string
            if ($this->endsWith($key, "phone")) {
                $model[$key] = $value . "\n";
            }

            else if ($this->endsWith($key, "_timestamp")) {
                if (!is_numeric($value) && !empty($value))
                    $model[$key] = strtotime($value);
            }
        }

        // $icon_file = $model['avatar_file'];
        // $model['icon_file_url'] = base_url("uploads/").$icon_file;

        //remove unused fields
        $unusedFields = $this->sensitiveFields;
        if(!empty($unusedFields))
        foreach ($unusedFields as $field)
            if (isset($model[$field]))
                unset($model[$field]);

        return $model;
    }
    
    
    
}
