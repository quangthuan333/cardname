<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

$isStaging = IS_STAGING;
$isProduction = IS_PRODUCTION;

if($isProduction){
	defined('EW_CONN_HOST') or define('EW_CONN_HOST', 'localhost');
	defined('EW_CONN_PORT') or define('EW_CONN_PORT', 3306);
	defined('EW_CONN_USER') or define('EW_CONN_USER', 'namecard_dev');
	defined('EW_CONN_PASS') or define('EW_CONN_PASS', 'WegkJ4soShqflyCt');
	defined('EW_CONN_DB') or define('EW_CONN_DB', 'namecard_dev');
}
	
else{
	defined('EW_CONN_HOST') or define('EW_CONN_HOST', 'localhost');
	defined('EW_CONN_PORT') or define('EW_CONN_PORT', 3306);
	defined('EW_CONN_USER') or define('EW_CONN_USER', 'quangthuan');
	defined('EW_CONN_PASS') or define('EW_CONN_PASS', '0tpnF6TPwlOm3Tzs');
	defined('EW_CONN_DB') or define('EW_CONN_DB', 'cardname');
}
$db['default'] = array(
	'dsn'	=> '',
	'hostname' => EW_CONN_HOST,
	'username' => EW_CONN_USER,
	'password' => EW_CONN_PASS,
	'port'     => EW_CONN_PORT,
	'database' => EW_CONN_DB,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => FALSE,
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);


// 3rd party Integrations
if ($isProduction)
{
	defined('DEBUG') or define('DEBUG', false);
	defined('LOCALHOST') or define('LOCALHOST', false);
}
else
{
	defined('DEBUG') or define('DEBUG', TRUE);
	defined('LOCALHOST') or define('LOCALHOST', TRUE);
}