<?php
class Website extends CI_Model{
    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function getWebsite(){
        // $sql = 'SELECT value FROM setting';
        $query=$this->db->get("setting");
        // $query=$this->db->query($sql);
        if(empty($query))
            return NULL;

        $array=$query->result_array();
        if(empty($array))
            return NULL;
        return $array;
    }
}
?>