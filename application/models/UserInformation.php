<?php
class UserInformation extends MY_Model
{
    protected $table_name = 'user_information';
    protected $sensitiveFields = ["user_id", "value", "type", "slug", "prefix","label"];
    
    public function getWithUserId($id)
    {
        if (empty($id))
            return NULL;

        $sql = "SELECT * 
                FROM user_information
                LEFT JOIN information_type ON information_type.`slug` = user_information.`type`
                WHERE user_id = $id";

        $models = $this->db->query($sql);
        if (empty($models))
            return NULL;

        $array = $models->result_array();
        if (empty($array))
            return NULL;

        //massage model
        foreach ($array as &$model)
            $this->massageModel($model);

        return $array;
    }

    protected function massageModel(&$model)
    {

        //Prefer `label` over `name`
        if (empty(trim($model['label'])))
            $model['label'] = trim($model['name']);

        //prefix = 'tel://'    value = '(+84) 8162 153 141'                => display_value = "(+84) 8162 153 141"         action_url = "tel://+848162153141"
        //prefix = 'tel://'    value = '+848162153141'                     => display_value = "+848162153141"              action_url = "tel://+848162153141"
        //prefix = 'tel://'    value = 'tel://+848162153141'               => display_value = "+848162153141"              action_url = "tel://+848162153141"
        //prefix = 'https://'  value = 'https://facebook.com/torinnguyen'  => display_value = "facebook.com/torinnguyen"   action_url = "facebook://xxxxx/torinnguyen"
        //prefix = 'https://'  value = 'facebook.com/torinnguyen'          => display_value = "facebook.com/torinnguyen"   action_url = "https://facebook.com/torinnguyen"
        //prefix = ' '         value = '123 Wall Street'                   => display_value = "123 Wall Street"            action_url = NULL
        

        $prefix = trim($model['prefix']);
        $value = trim($model['value']);

        //remove prefix from value, if any
        $display_value = str_replace($prefix, "", $value);
        $model['display_value'] = $display_value;  

        //process telephone format
        if (contains($prefix, 'tel:')) {
            $value = str_replace("(", "", $value);
            $value = str_replace(")", "", $value);
            $value = str_replace(" ", "", $value);
            $value = str_replace("-", "", $value);
            $value = str_replace("_", "", $value);
        }

        $value = str_replace($prefix, "", $value);

        $model['action_url'] = NULL;
        if (!empty($prefix))
            $model['action_url'] = $prefix . $value;

        //remove unused fields
        parent::massageModel($model);

        return $model;
    }
}
?>